# Motores Gráficos II - TP01 & TP03 - Runner

Programación, shaders y modelos 3D hechos por mi.

### Consigna TP01
Desarrolle sobre la demo presentada los siguientes puntos:

- Detener el paralax al perder.
- Redondear la distancia.
- Crear 2 nuevos enemigos.
- Agregar un powerup.
- Bajar la velocidad del paralax.
- Realizarle comentarios al código.
- Agregar una nueva mecánica.

Demo Original: https://github.com/Hjupol/runner.git

### Consigna TP03
Con los visto en la clase sobre Estéticas desarrollar un proyecto donde el foco del diseño sea la estética.
- Agregar 3 shaders.
- El proyecto debe tener un estilo y estética claros que se transmitan al jugador a través del gameplay.


### Video resolución TP01
[![Watch the video](https://img.youtube.com/vi/IsJs0RCZQuE/hqdefault.jpg)](https://youtu.be/IsJs0RCZQuE)

### Resolución TP03
[Documento](https://drive.google.com/file/d/1yz8Tw34hh4rOT-ePPgxpPwdbqRmWpDD0/view?usp=sharing)