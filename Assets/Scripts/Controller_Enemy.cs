﻿using UnityEngine;

public class Controller_Enemy : MonoBehaviour
{
    public static float enemyVelocity;
    private Rigidbody rb;

    //obtengo el component rigidbody
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    //agrego fuerza al rigidbody para que se mueva para la izquierda y ejecuto metodo OutOfBounds
    void Update()
    {
        rb.AddForce(new Vector3(-enemyVelocity, 0, 0), ForceMode.Force);
        OutOfBounds();
    }

    //al estar en la posicion -20 del eje X se autodestruye
    public void OutOfBounds()
    {
        if (this.transform.position.x <= -20)
        {
            Destroy(this.gameObject);
        }
    }
}
