using UnityEngine;

public class BalaEnemigo_Controller : MonoBehaviour
{
    //Al colisionar destruye al jugador, activa el GameOver y se autodestruye al colisionar con cualquier cosa
    private void OnCollisionEnter(Collision collision)
    {
        Destroy(this.gameObject);

        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(collision.gameObject);
            Controller_Hud.gameOver = true;
        }
    }
}