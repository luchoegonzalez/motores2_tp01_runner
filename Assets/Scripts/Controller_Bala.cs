using UnityEngine;

public class Controller_Bala : MonoBehaviour
{
    //Al colisionar destruye a los enemigos comunes y se autodestruye al colisionar con cualquier cosa
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(collision.gameObject);
        }

        Destroy(this.gameObject);
    }
}
