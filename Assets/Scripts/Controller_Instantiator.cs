﻿using System.Collections.Generic;
using UnityEngine;

public class Controller_Instantiator : MonoBehaviour
{
    // variables relacionadas a spawnear a los enemigos
    public List<GameObject> enemies;
    public GameObject instantiatePos;
    public float respawningTimer;
    private float time = 0;

    void Start()
    {
        //al ser static seteo esta variable en cada Start (pertenece al script de control de enemigo)
        Controller_Enemy.enemyVelocity = 2;
    }

    //ejecuto dos metodos en update
    void Update()
    {
        SpawnEnemies();
        ChangeVelocity();
    }

    // a medida que pasa el tiempo aumenta la velocidad
    private void ChangeVelocity()
    {
        time += Time.deltaTime;
        Controller_Enemy.enemyVelocity = Mathf.SmoothStep(1f, 10f, time / 180f);
    }

    // dentro de la lista de enemigos selecciona uno al azar y lo spawnea entre un tiempo al azar
    private void SpawnEnemies()
    {
        respawningTimer -= Time.deltaTime;

        if (respawningTimer <= 0)
        {
            Instantiate(enemies[UnityEngine.Random.Range(0, enemies.Count)], instantiatePos.transform);
            respawningTimer = UnityEngine.Random.Range(2, 4);
        }
    }
}
