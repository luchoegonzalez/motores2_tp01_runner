﻿using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    // variables de fisicass
    private Rigidbody rb;
    public float jumpForce = 10;

    // variables para agacharse y saltar
    private float initialSize;
    private int i = 0;
    private bool floored;

    // variables para animacion de bola
    public GameObject bola;
    public float rapidezDeRotacion = 2;
    private float rapidezDeRotacionInicial;
    private float time = 0;

    // variables para disparar
    public GameObject balaPrefab;
    public float fuerzaBala = 10;
    public Transform instanciadorDeBalas;
    private int cantidadDeBalas = 5;

    private void Start()
    {
        // seteo las variables y el texto de la cantidad de balas
        rb = GetComponent<Rigidbody>();
        initialSize = rb.transform.localScale.y;
        rapidezDeRotacionInicial = rapidezDeRotacion;

        actualizarTextosDeBalas();
    }

    // en cada frame chequeamos los inputs y haemos que la bola rote para dar sensacion de movimiento
    void Update()
    {
        GetInput();

        time += Time.deltaTime;
        rapidezDeRotacion = Mathf.SmoothStep(rapidezDeRotacionInicial * 1f, rapidezDeRotacionInicial * 10f, time / 180f);
        bola.transform.Rotate(new Vector3(rapidezDeRotacion * Time.deltaTime, 0,0));
    }

    // Durante el chequeo de input se pueden ejecutar tres metodos
    private void GetInput()
    {
        Jump();
        Duck();
        Shoot();
    }

    // El personaje salta al apretar la W (añadiento una fuerza de impulso)
    private void Jump()
    {
        if (floored)
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    // Al apretar la S el personaje se achica en el eje Y para simular agacharse, y sino vuelve a su tamaño original.
    // Si esta en el aire y se aprieta S baja con la misma fuerza del salto a la inversa
    private void Duck()
    {
        if (floored)
        {
            if (Input.GetKey(KeyCode.S))
            {
                if (i == 0)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, rb.transform.localScale.y / 2, rb.transform.localScale.z);
                    
                    transform.position = new Vector3(transform.position.x, transform.position.y - 0.25f, transform.position.z);
                    i++;
                }
            }
            else
            {
                if (rb.transform.localScale.y != initialSize)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, initialSize, rb.transform.localScale.z);
                    i = 0;
                }
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                rb.AddForce(new Vector3(0, -jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    // Al apretar el espacio se instancia un prefab de bala que se le aplica una fuerza, luego de un tiempo se destruye.
    // Tambien descuenta la bala del total y actaliza el texto
    private void Shoot()
    {
        if (Input.GetKeyDown(KeyCode.Space) && cantidadDeBalas > 0)
        {
            GameObject bala = Instantiate(balaPrefab, instanciadorDeBalas.position, Quaternion.Euler(0, 0, 90));
            bala.GetComponent<Rigidbody>().AddForce(Vector3.right * fuerzaBala, ForceMode.Impulse);
            Destroy(bala, 0.9f);
            cantidadDeBalas--;
            actualizarTextosDeBalas();
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        // si colisiono con un enemigo comun o inmortal ejecuto el metodo de gameover y este objeto se autodestruye
        if (collision.gameObject.CompareTag("Enemy") || collision.gameObject.CompareTag("Inmortal"))
        {
            Destroy(this.gameObject);
            Controller_Hud.gameOver = true;
        }

        // chequeo si estoy tocando el piso
        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = true;
        }
    }

    // chequeo si dejo de tocar el piso
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // si colisiono con el objeto Ammo, me da 5 balas, se destruye dicho objeto y actualizo el texto de balas en pantalla
        if (other.CompareTag("Ammo"))
        {
            Destroy(other.gameObject);
            cantidadDeBalas = cantidadDeBalas + 5;
            actualizarTextosDeBalas();
        }

        // si colisiono con el caracol relentizo el tiempo y se destruye dicho objeto.
        if (other.CompareTag("Caracol"))
        {
            Destroy(other.gameObject);

            // divido por la mitad los valores siguientes y al cabo de cierto tiempo ejecuto la funcion que los resetea.
            // Cancelo el invoke al inicio para que en caso de agarrar 2 caracoles seguidos no haya errores
            CancelInvoke("StopSlowDown");
            Time.timeScale = 0.5f;
            Time.fixedDeltaTime /= 2;
            Parallax.timeMultiplier = 0.5f;
            Invoke("StopSlowDown", 2.5f);
        }
    }

    private void StopSlowDown()
    {
        // devuelvo los valores de tiempo a su estado original
        Time.timeScale = 1f;
        Time.fixedDeltaTime = 0.02f;
        Parallax.timeMultiplier = 1f;
    }

    // actualizo la cantidad de balas para que sea mostrado en pantalla
    private void actualizarTextosDeBalas()
    {
        Controller_Hud.cantidadDeBalas = cantidadDeBalas;
        Controller_Hud.ammoUpdate = true;
    }
}
