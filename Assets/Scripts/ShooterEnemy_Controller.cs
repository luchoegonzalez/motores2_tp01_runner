using UnityEngine;

public class ShooterEnemy_Controller : MonoBehaviour
{
    // variables para instanciar balas 
    public GameObject balaPrefab;
    public Transform instanciadorDeBalas;

    // bool para chequear que solo dispare una vez
    bool disparoRealizado = false;

    // variables para control de rapidez de bala
    float rapidezBala = 0.2f;
    private float time = 0;

    private void Update()
    {
        enPosicionDeAtaque();
        rapidezBala = Mathf.SmoothStep(0.25f, 2.5f, time / 180f);
    }

    // al entrar en pantalla dispara solo una bala hacia el jugador
    void enPosicionDeAtaque()
    {
        if (this.transform.position.x <= 20 && !disparoRealizado)
        {
            GameObject bala = Instantiate(balaPrefab, instanciadorDeBalas.position, Quaternion.Euler(0, 0, 90));
            bala.GetComponent<Rigidbody>().AddForce(Vector3.left * rapidezBala, ForceMode.Impulse);
            Destroy(bala, 1.2f);
            disparoRealizado = true;
        }
    }
}
