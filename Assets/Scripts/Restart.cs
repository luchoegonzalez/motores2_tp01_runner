﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{
    void Update()
    {
        GetInput();
    }

    private void GetInput()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            //si aprieto la R vuelvo a cargar la escena reiniciando el nivel
            Time.timeScale = 1;
            SceneManager.LoadScene(0);
        }
    }
}
 