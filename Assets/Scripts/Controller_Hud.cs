﻿using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour
{
    //textos en pantalla
    public static bool gameOver = false;
    public Text distanceText;
    public Text gameOverText;
    public Text ammoText;

    //distancia recorrida
    private float distance = 0;

    //variables de municion
    public static bool ammoUpdate = false;
    public static int cantidadDeBalas;

    //seteo las variables y redondeo la distancia con el formato F0 para que no se vea como un decimal
    void Start()
    {
        gameOver = false;
        distance = 0;
        distanceText.text = distance.ToString("F0");
        gameOverText.gameObject.SetActive(false);
    }

    void Update()
    {
        //si perdes el tiempo se pone en 0 y muestra la distancia recorrida con el texto Game Over
        if (gameOver)
        {
            Time.timeScale = 0;
            gameOverText.text = "Game Over \n Total Distance: " + distance.ToString("F0");
            gameOverText.gameObject.SetActive(true);

            // pauso el movimiento del parallax con este multiplicador en 0
            Parallax.timeMultiplier = 0;
        }
        else
        {
            distance += Time.deltaTime;
            distanceText.text = distance.ToString("F0");
        }

        //muestro la cantidad de balas que tiene el jugador
        if (ammoUpdate)
        {
            ammoText.text = "Balas: " + cantidadDeBalas.ToString();
            ammoUpdate = false;
        }
    }
}
