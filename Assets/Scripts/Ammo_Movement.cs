using UnityEngine;

public class Ammo_Movement : MonoBehaviour
{
    public float rapidezAnimacion;

    // hago girar en loop al objeto
    void Update()
    {
        transform.Rotate(0, rapidezAnimacion * Time.deltaTime, 0);
    }
}
