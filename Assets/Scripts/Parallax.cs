﻿using UnityEngine;

public class Parallax : MonoBehaviour
{
    // variables que supongo que querian usar pero descartaron
    private float length, startPos;

    // variables para mover a los objetos
    public float parallaxEffect;
    private float time = 0;

    // multiplicador de tiempo para modificar tiempo del parallax desde otros scripts
    public static float timeMultiplier = 1;

    // valor inicial de parallax para despues modificarlo
    private float parallaxEffectInicial;

    void Start()
    {
        // seteo las variables
        startPos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;

        parallaxEffectInicial = parallaxEffect;
        timeMultiplier = 1;
    }

    void Update()
    {
        // hago que el objeto se mueva hacia la izquierda el parallaxEffect que le asigno desde el Inspector y el timeMultiplier
        transform.position = new Vector3(transform.position.x - parallaxEffect * timeMultiplier, transform.position.y, transform.position.z);

        // si llega a cierto valor se reinicia la posicion
        if (transform.localPosition.x < -20)
        {
            transform.localPosition = new Vector3(20, transform.localPosition.y, transform.localPosition.z);
        }

        // el parallax va aumentando la velocidad a medida que pasa el tiempo
        time += Time.deltaTime;
        parallaxEffect = Mathf.SmoothStep(parallaxEffectInicial * 1f, parallaxEffectInicial * 8f, time / 180f);
    }
}
